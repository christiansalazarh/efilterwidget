// el codigo JS del widget
//
var EFilterWidget = function(options){

	var divMayor = $('#'+options.id);
	
	var ajaxcmd = function(action,postdata,callback){
		var result=false;
		var nocache=function(){
			var dateObject = new Date();
			var uuid = dateObject.getTime();
			return "&nocache="+uuid;
		}
		jQuery.ajax({
			url: action+nocache(),
			type: 'post',
			async: false,
			contentType: "application/json",
			data: postdata,
			success: function(data, textStatus, jqXHR){
				result = data;
				if(callback != null)
					callback(true, data);
			},
			error: function(jqXHR, textStatus, errorThrown){
				callback(false, jqXHR.responseText);
				return false;
			},
		});
		return result;
	}

	// FIND BUTTON CLICK EVENT
	$('#'+options.findid).click(function(){
		var urlArgs='';
		var kk='';
		var c='';
		$.each(options.keys, function(index,key){
			var value = $('#'+key).find('.efilterwidget-value').val();
			urlArgs += '&'+key+'='+value;
			kk += c+key;
			c=',';
		});		
	    // plus special argument: keys
		urlArgs += '&keys='+kk;
		urlArgs += '&arg='+$('#'+options.argumentId).val();
		
		var loading = $('#'+options.loading);
		var find = $('#'+options.findid);
		var ok = $('#'+options.findok);
		var sel = $('#'+options.sel);
		loading.show();
		ok.attr('disabled','disabled');
		find.attr('disabled','disabled');
		sel.attr('disabled','disabled');
		sel.find('option').each(function(){ $(this).remove(); });

		ajaxcmd(options.action+urlArgs,'',function(result, data){
			find.attr('disabled',null);
			sel.attr('disabled',null);
			ok.attr('disabled',null);
			loading.hide();
			if(result == true){
				// data comming from server, fill select options
				// and wait for user interaction
				//
				// DATA EXPECTED HERE:  JSON ARRAY, key=>value
				var opts="<option value=''>"+options.pleaseSelect+"</option>";
				$.each(data,function(value,text){
					opts += "<option value='"+value+"'>"+text+"</option>";					
				});
				sel.html(opts);
			}else{
				options.onError(data);
			}
		});
	});

	$('#'+options.findok).click(function(){
		var value = $('#'+options.sel).val();
		if(value != ''){
			if(options.receptorId != '')
				$('#'+options.receptorId).val(value);
			options.onSuccess(value);
		}
	});

};
