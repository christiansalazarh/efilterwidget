<?php
/**
 * EFilterWidget 
 *
 *	Please refer to README about details.
 *
 *
 * @uses CWidget
 * @version 1.0 
 * @author Christian Salazar <christiansalazarh@gmail.com> 
 * @license FREE BSD
 */
class EFilterWidget extends CWidget {

	public $id;					// HTML MAIN DIV ID
	public $findButtonLabel;	// "Find >>"
	public $okButtonLabel;		// "OK"
	public $pleaseSelectText;	// "-please select-"
	public $fields;				// array. see doc.
	public $action;				// action array URL

	public $actionInputArgumentID; // input tag ID, containing data 
								 // to be attached to the action URL
								 // so the action will read it and acts
								 // in response to this argument.
	
	// WAW1: using an ID as receptor for selected value uppon user clicks "OK"
	public $receptorId; 
	// WAY2: using onSuccess event handler fired when user clicks "OK"
	public $onSuccess;			// successfull user selection handler
	// onError will always be called on any error.
	public $onError;			// error handler
	public $backgroundColor;	// widget bg color. default none.


	private $_baseUrl;
	public $debug;

	public function init(){
		parent::init();
		if($this->pleaseSelectText == null)
			$this->pleaseSelectText = '-please select-';
		if($this->okButtonLabel==null)
			$this->okButtonLabel='OK';
		if($this->findButtonLabel==null)
			$this->findButtonLabel='Find>>';
		if($this->onSuccess == null)
			$this->onSuccess = "function(){}";
		if($this->onError == null)
			$this->onError = "function(){}";
		if($this->backgroundColor == null)
			$this->backgroundColor = '';
	}

	public function run(){
		$this->_prepareAssets();

		$keys = array();
		$html = "<ul class='efilterwidget-ul'>";
		foreach($this->fields as $key=>$field){
			$keys[] = $key;
			$html .= $this->_buildFieldUI(
				$key,
				$field['ui'], 
				$field['label'], 
				$field['value'],
				isset($field['options']) ? $field['options'] : array(),
				isset($field['size']) ? $field['size'] : ""
			);
		}
		$html .= "</ul>";

		$findid = $this->id."-find";
		$findsel = $this->id."-sel";
		$findok = $this->id."-ok";
		$imgloading=$this->id."-loading";
		$loading = $this->_baseUrl . '/loading.gif';

		$ss="";
		if($this->backgroundColor != '')
			$ss = "style='background-color: ".$this->backgroundColor.";'";

		// main html layout for this UI component
		//
		echo 
"
<!-- efilterwidget extension by: christiansalazarh@gmail.com -->
<div id={$this->id} class='efilterwidget' {$ss}>
	{$html}
	<div class='finder'>
		<input id='{$findid}' type='button' value='"
			.CHtml::encode($this->findButtonLabel)."'>
		<select id='{$findsel}' class='efilterwidget-results'></select>
		<input id='{$findok}' class='okbutton' type='button' value='"
			.CHtml::encode($this->okButtonLabel)."'>
		<img src='{$loading}' style='display: none;' id='{$imgloading}'>
	</div>
</div>
";

		// options passed by to main jQuery script
		//
		
		$options = CJavaScript::encode(array(
			'action'=>CHtml::normalizeUrl($this->action),
			'id'=>$this->id,
			'keys'=>$keys,
			'findid'=>$findid,
			'sel'=>$findsel,
			'findok'=>$findok,
			'loading'=>$imgloading,
			'pleaseSelect'=>$this->pleaseSelectText,
			'receptorId'=>$this->receptorId,
			'argumentId'=>$this->actionInputArgumentID,
			'onSuccess'=>new CJavaScriptExpression($this->onSuccess),
			'onError'=>new CJavaScriptExpression($this->onError),
		));

		$var_id = rand(1000,99999);
		Yii::app()->getClientScript()->registerScript(
			"efilterwidget_script_".$var_id,
				"var efilterwidget_{$var_id} = new EFilterWidget({$options});");
		
	}// end run()

	public function _prepareAssets(){
		$localAssetsDir = dirname(__FILE__) . '/assets';
		$this->_baseUrl = Yii::app()->getAssetManager()->publish(
				$localAssetsDir);

		if($this->debug == true)
		$this->_baseUrl = 'protected/extensions/efilterwidget/assets';

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
		foreach(scandir($localAssetsDir) as $f){
			$_f = strtolower($f);
			if(strstr($_f,".swp"))
				continue;
			if(strstr($_f,".js"))
				$cs->registerScriptFile($this->_baseUrl."/".$_f);
			if(strstr($_f,".css"))
				$cs->registerCssFile($this->_baseUrl."/".$_f);
		}
	}


	private function _buildFieldUI($key,$ui, $label, $value, $options, $size){
		if($ui == 'text'){
			return $this->_buildTextUI($key,$label, $value, $size);
		}elseif($ui == 'list'){
			return $this->_buildListUI($key,$label, $value, $options, $size);
		}
		else
			throw new Exception("Invalid argument value for UI."
					." must be 'text' or 'list'");
	}

	private function _buildTextUI($key, $label, $value, $size){
		$ss=''; if($size != '') $ss = "size={$size}";
		return 
"
<li class='efilterwidget-ui-text' id='{$key}'>
   <div>{$label}</div>
   <input type='text' class='efilterwidget-value' {$ss} value='{$value}'>
</li>
";	
	}

	private function _buildListUI($key, $label, $value,$options, $size){
		$list = CHtml::dropDownList($key.'-list',$value,$options,
			array('class'=>'efilterwidget-value'));
		return
"
<li class='efilterwidget-ui-list' id='{$key}'>
   <div>{$label}</div>
   {$list}
</li>
";	
	}

}
